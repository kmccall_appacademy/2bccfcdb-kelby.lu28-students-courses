class Student

  attr_reader :first_name, :last_name, :courses

  def initialize(first, last)
    @first_name = first
    @last_name = last
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    @courses.each do |course|
      if course.conflicts_with?(new_course)
        raise "ERROR: schedule conflict"
      end
    end
    @courses.push(new_course) #unless @courses.include?(new_course)
    new_course.students.push(self)
  end

  def course_load
    course_creds = Hash.new(0)

    courses.each do |course|
      course_creds[course.department] += course.credits
    end

    course_creds
  end


end
